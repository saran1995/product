<?php

use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DB;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\Queries\SQLSelect;
use SilverStripe\ORM\Queries\SQLDelete;
use SilverStripe\ORM\Queries\SQLUpdate;
use SilverStripe\ORM\Queries\SQLInsert;
use SilverStripe\ORM\ArrayList;
use SilverStripe\Control\Controller;

class FormController extends ContentController
{   
    private static $allowed_actions = [
        'savedata', 'getdetail', 'deldetail'
    ];

    protected function init()
    {
        parent::init();

    }

    function pdet(){

        $players = Formmod::get();

        return $players;
    }

    function det()
    {
        return 'hai';
    }

    public function doSubmitForm($data, $form)
    {

        $comment = Formmod::create();
        $comment->PlayerNumber = $data['PlayerNumber'];
        $comment->FirstName = $data['FirstName'];
        $comment->LastName = $data['LastName'];
        $comment->write();


        // Using the Form instance you can get / set status such as error messages.
        $form->sessionMessage('Successful!', 'good');

        // After dealing with the data you can redirect the user back.
        return $this->redirect('FormController');
    }

    function savedata()
    {
        $id = $_POST['a_id'];
        if($id == '')
        {
            $comment = Formmod::create();
            $comment->PlayerNumber = $_POST['PlayerNumber'];
            $comment->FirstName = $_POST['FirstName'];
            $comment->LastName = $_POST['LastName'];
            $comment->write();
        }
        else
        {
            DB::query("UPDATE `formmod` SET `PlayerNumber`='".$_REQUEST['PlayerNumber']."', `FirstName`='".$_REQUEST['FirstName']."', `LastName`='".$_REQUEST['LastName']."' where `ID` = '".$id."'");
        }  
            
     
        return $this->redirect('FormController');
        
    }

    public function getdetail()
    {
        $id = $_POST['id'];
        $player = Formmod::get()->byID($id);

        echo $player->PlayerNumber.'|'.$player->FirstName.'|'.$player->LastName;
    }

    public function deldetail()
    {
        $id = $_POST['id'];
        DB::query("DELETE FROM `formmod` WHERE ID='".$id."'");
    }

    

    
}

?>
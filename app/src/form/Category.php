<?php
use SilverStripe\ORM\DataObject;

class Category extends DataObject 
{
    private static $db = [
        'CategoryName' => 'Varchar(255)'
    ];

    private static $has_many = [
        'Products' => Product::class,
    ];
}

?>
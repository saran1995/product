<?php
use SilverStripe\ORM\DataObject;

class Formmod extends DataObject 
{
    private static $db = [
        'PlayerNumber' => 'Int',
        'FirstName' => 'Varchar(255)',
        'LastName' => 'Text'
    ];

    private static $has_one = [
        'FormController' => FormController::class
    ];
}

?>
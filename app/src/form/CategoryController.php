<?php

use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DB;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\Queries\SQLSelect;
use SilverStripe\ORM\Queries\SQLDelete;
use SilverStripe\ORM\Queries\SQLUpdate;
use SilverStripe\ORM\Queries\SQLInsert;
use SilverStripe\ORM\ArrayList;
use SilverStripe\Control\Controller;

class CategoryController extends ContentController
{   
    private static $allowed_actions = [
        'get_category', 'get_products', 'insertCategory', 'category_name_unique', 'getCategoryDetail', 'deleteCategory'
    ];

    protected function init()
    {
        parent::init();

    }

    //To get category details
    function get_category(){
        $cats = Category::get();

        return $cats;
    }

    //To get products
    function get_products($data=array()){
        $Params = $data->postVars();
        $cid = $Params['cid'];
        
        $html = '<tr><th>Products</th></tr>';
        if($cid != 0)
        {
           $cat = Category::get()->byId($cid);
            if($cat->Products()->count() > 0)
            {
                foreach($cat->Products() as $prod) {
                    $html .= "<tr><td>$prod->ProductName</td></tr>";
                }
            }
            else
            {
                $html .= '<td>No Products Available</td>';
            } 
        }
        else
        {
            $html .= '<td>No Products Available</td>';
        }    

        echo $html;

    }


    //To check category name unique
    function category_name_unique($data=array())
    {
        $Params = $data->postVars();
        $val = $Params['val'];
        $id = $Params['c_id'];
        if($id == 0)
        {
            $sqlQuery = new SQLSelect();
            $sqlQuery->setFrom('category');
            $sqlQuery->selectField('*');
            $sqlQuery->addWhere(array('CategoryName' => $val));
            
            $result = $sqlQuery->execute();
            if($result->numRecords() > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            $sqlQuery = new SQLSelect();
            $sqlQuery->setFrom('category');
            $sqlQuery->selectField('*');
            $sqlQuery->addWhere(array('CategoryName' => $val, 'ID != ?' => $id));
            
            $result = $sqlQuery->execute();
            if($result->numRecords() > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }  
        
    }

    //To insert category
    function insertCategory($data=array())
    {
        $Params = $data->postVars();
        $id = $Params['c_id'];
        if($id == 0)
        {
            $comment = Category::create();
            $comment->CategoryName = $Params['cname'];
            $comment->write();
        }
        else
        {
            $update = SQLUpdate::create('"category"')->addWhere(array('ID' => $id));

            // assigning a list of items
            $update->addAssignments(array(
                '"CategoryName"' => $Params['cname']
            ));
            $update->execute();
        }  
            
     
        return $this->redirect('category');
        
    }

    //To get category details by id for update
    public function getCategoryDetail($data=array())
    {
        $Params = $data->postVars();
        $id = $Params['id'];
        $category = Category::get()->byID($id);
        
        echo $category->CategoryName;
    }

    //To delete category
    public function deleteCategory($data=array())
    {
        $Params = $data->postVars();
        $id = $Params['id'];
        $query = SQLDelete::create()
            ->setFrom('"category"')
            ->setWhere(array('"category"."ID"' => $id));
        $query->execute();
    }

    
}

?>
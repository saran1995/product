<?php

use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DB;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\Queries\SQLSelect;
use SilverStripe\ORM\Queries\SQLDelete;
use SilverStripe\ORM\Queries\SQLUpdate;
use SilverStripe\ORM\Queries\SQLInsert;
use SilverStripe\ORM\ArrayList;
use SilverStripe\Control\Controller;

class ProductController extends ContentController
{   
    private static $allowed_actions = [
        'product_details', 'insertProduct', 'getProductDetail', 'deleteProduct', 'product_name_unique'
    ];

    protected function init()
    {
        parent::init();

    }

    //To get product details for list
    function product_details(){
        $sqlQuery = new SQLSelect();
        $sqlQuery->setFrom('product');
        $sqlQuery->selectField('product.*');
        $sqlQuery->selectField('category.CategoryName');
        $sqlQuery->addLeftJoin('category','"product"."CategorysID" = "category"."ID"');
        
        $result = $sqlQuery->execute();

        $list = ArrayList::create();
        foreach($result as $row) {
            $list->push($row);
        }

        return $list;
    }

    //To get category details
    function category_details(){

        $cats = Category::get();

        return $cats;
    }

    //To check product name unique
    function product_name_unique($data=array())
    {
        $Params = $data->postVars();
        $val = $Params['val'];
        $id = $Params['p_id'];
        if($id == 0)
        {
            $sqlQuery = new SQLSelect();
            $sqlQuery->setFrom('product');
            $sqlQuery->selectField('*');
            $sqlQuery->addWhere(array('ProductName' => $val));
            
            $result = $sqlQuery->execute();
            if($result->numRecords() > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            $sqlQuery = new SQLSelect();
            $sqlQuery->setFrom('product');
            $sqlQuery->selectField('*');
            $sqlQuery->addWhere(array('ProductName' => $val, 'ID != ?' => $id));
            
            $result = $sqlQuery->execute();
            if($result->numRecords() > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }  
        
    }

    //To insert product
    function insertProduct($data=array())
    {
        $Params = $data->postVars();
        $id = $Params['p_id'];
        if($id == 0)
        {
            $comment = Product::create();
            $comment->ProductName = $Params['pname'];
            $comment->CategorysID = $Params['cat'];
            $comment->write();
        }
        else
        {
            $update = SQLUpdate::create('"product"')->addWhere(array('ID' => $id));

            // assigning a list of items
            $update->addAssignments(array(
                '"ProductName"' => $Params['pname'],
                '"CategorysID"' => $Params['cat']
            ));
            $update->execute();
        }  
            
     
        return $this->redirect('product');
        
    }

    //To get product details by id for update
    public function getProductDetail($data=array())
    {
        $Params = $data->postVars();
        $id = $Params['id'];
        $product = Product::get()->byID($id);
        $html = '';

        $cats = Category::get();

        foreach ($cats as $key => $value) {
            $html .= "<option  ";
            if($value->ID == $product->CategorysID)
            {
              $html .= "selected";  
            }
            
            $html .= " value='$value->ID' >$value->CategoryName</option>";
        }

        echo $product->ProductName.'|'.$html;
    }

    //To delete product
    public function deleteProduct($data=array())
    {
        $Params = $data->postVars();
        $id = $Params['id'];
        $query = SQLDelete::create()
            ->setFrom('"product"')
            ->setWhere(array('"product"."ID"' => $id));
        $query->execute();
    }

    

    
}

?>
<?php
use SilverStripe\Forms\TextField;

class PlayerNumber extends TextField
{
    public function validate($validator)
    {
        if (!is_numeric($this->value)) {
            $validator->validationError(
                $this->name, 'Not a number. This must be between 2 and 5', 'validation', false
            );
            
            return false;
        } 

        return true;
    }
}

?>o